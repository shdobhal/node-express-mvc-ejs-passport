// set up a temporary (in memory) database
const Datastore = require('nedb')
const LOG = require('../utils/logger.js')
const puppies = require('../data/puppies.json')
const users = require('../data/users.json')
const userController = require('../controllers/user')

module.exports = (app) => {
  LOG.info('START seeder.')
  const db = {}
  db.puppies = new Datastore()
  db.users = new Datastore()

  db.puppies.loadDatabase()
  db.users.loadDatabase()

  // insert the sample data into our data stores
  db.puppies.insert(puppies)
  db.users.insert(users.data)

  // initialize app.locals (these objects will be available to our controllers)
  app.locals.puppies = db.puppies.find(puppies)
  LOG.debug(`${puppies.length} puppies seeded`)

  app.locals.users = db.users.find(users)
  LOG.debug(`${users.length} users seeded`)

  users.forEach((user) => {
    userController.newUser(user)
  })

  LOG.info('END Seeder. Sample data read and verified.')
}
