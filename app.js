/**
 * @file app.js
 * The starting point of the application.
 * Express allows us to configure our app and use
 * dependency injection to add it to the http server.
 *
 * The server-side app starts and begins listening for events.
 *
 */

// Module dependencies
const express = require('express')
const http = require('http')
const path = require('path')
const engines = require('consolidate')
const dotenv = require('dotenv')
const routesMain = require('./routes/index.js')
const routesUser = require('./routes/user.js')
const errorHandler = require('errorhandler')
const favicon = require('serve-favicon')
const expressLayouts = require('express-ejs-layouts')
const expressValidator = require('express-validator')
const expressStatusMonitor = require('express-status-monitor')
const bodyParser = require('body-parser')
const LOG = require('./utils/logger.js')

global.passport = require('passport')
const compression = require('compression')
const session = require('express-session')
const MongoStore = require('connect-mongo')(session)
const mongoose = require('mongoose')
const flash = require('express-flash')

// Load environment variables from .env file, where API keys and passwords are configured.
// dotenv.load({ path: '.env.example' })
dotenv.load({ path: '.env' })
LOG.info('Environment variables loaded.')

// app variables
const DEFAULT_PORT = 8089
const isProduction = process.env.NODE_ENV === 'production'
LOG.info('Environment isProduction = ', isProduction)

// global config files
global.ensureAuthenticated = require('./config/ensureAuthenticated')

// Connect to MongoDB............................
if (isProduction) {
  mongoose.Promise = global.Promise
  mongoose.connect(process.env.MONGODB_URI_ATLAS)
  mongoose.connection.on('error', () => {
    console.log('%s MongoDB connection error. Please make sure Atlas MongoDB is running.', chalk.red('✗'))
    process.exit()
  })
  LOG.info('Connected to Atlas MongoDB')
} else {
  mongoose.Promise = global.Promise
  mongoose.connect(process.env.MONGODB_URI)
  mongoose.connection.on('error', () => {
    console.log('%s MongoDB connection error. Please make sure local MongoDB is running.', chalk.red('✗'))
    process.exit()
  })
  LOG.info('Connected to local MongoDB')
}

// create express app ..................................
const app = express()

// configure app.settings.............................
app.set('port', process.env.PORT || DEFAULT_PORT)

// set the root view folder
app.set('views', path.join(__dirname, 'views'))

// specify desired view engine
app.set('view engine', 'ejs')
app.engine('ejs', engines.ejs)

// configure middleware.....................................................
app.use(favicon(path.join(__dirname, '/public/images/favicon.ico')))
app.use(expressStatusMonitor())
app.use(compression())

// log calls
app.use((req, res, next) => {
  LOG.debug('%s %s', req.method, req.url)
  next()
})

// specify various resources and apply them to our application
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(expressValidator())
app.use(express.static(path.join(__dirname, 'public'), { maxAge: 31557600000 }))
app.use(expressLayouts)
app.use(errorHandler()) // load error handler

app.use(session({
  resave: true,
  saveUninitialized: true,
  secret: process.env.SESSION_SECRET,
  store: new MongoStore({
    url: process.env.MONGODB_URI_ATLAS || process.env.MONGOLAB_URI,
    autoReconnect: true
  })
}))
app.use(global.passport.initialize())
app.use(global.passport.session())

app.use(flash())

app.use((req, res, next) => {
  res.locals.user = req.user
  next()
})

// load routing
app.use('/', routesMain)
app.use('/', routesUser)
LOG.info('Loaded routing.')

// handle page not found errors
app.use((req, res) => { res.status(404).render('404.ejs') })

// initialize data ............................................
require('./utils/seeder.js')(app)  // load seed data

// start Express app
app.listen(app.get('port'), () => {
  console.log('App is running at http://localhost:%d in %s mode', app.get('port'), app.get('env'))
  console.log('  Press CTRL-C to stop\n')
})

module.exports = app
